#!/bin/sh -x

CONFIG="${CONFIG_LOCATION}/${CONFIG_DOMAIN}"
LOG_OPTIONS="-Dlog4j.configurationFile=target/log4j2_local.xml"
: "${JAVA_MEM=1228}"
JAVA_OPTIONS="-Xmx${JAVA_MEM}m"

exec java $LOG_OPTIONS $JAVA_OPTIONS -jar target/file-transfer.jar \
  -config "config" \
  -logs "logs"
