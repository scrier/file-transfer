FROM java:8u91-jre

COPY target/file-transfer.jar /
COPY run.sh /run.sh

CMD chmod +x /run.sh && chmod +x file-transfer.jar && /run.sh
