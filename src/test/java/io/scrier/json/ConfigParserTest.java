package io.scrier.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static junit.framework.Assert.assertEquals;


/**
 * Created by scrier on 25/09/16.
 */
public class ConfigParserTest {

    private static final Logger LOGGER = LogManager.getLogger(ConfigParserTest.class);

    @Test
    public void testCreateFile() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        Config c = new Config()
                .setLogConfig(new LogConfig()
                        .setCopiedFiles(true)
                        .setMissedFiles(false)
                        .setSourceFiles(true)
                        .setDebugFileOperations(true))
                .setDelay(1300)
                .addSource("~Storage/transmission/download/completed")
                .addSource("Another source")
                .addSeries(new Series()
                        .setName("NCIS")
                        .setMatcher("NCIS.*")
                        .setTarget("/downloads/series/NCIS"))
                .addSeries(new Series()
                        .setName("Kalle")
                        .setMatcher("Kalle.*")
                        .setTarget("/downloads/series/Kalle"))
                .setMovies(new Movies()
                        .setName("Movies")
                        .setSizeGB(4.2)
                        .setTarget("/downloads/movies"));

        String s = null;
        try {
            s = mapper.writeValueAsString(c);
        }
        catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        assertEquals("{\n" +
                "  \"delay\" : 1300,\n" +
                "  \"sources\" : [ \"~Storage/transmission/download/completed\", \"Another source\" ],\n" +
                "  \"series\" : [ {\n" +
                "    \"name\" : \"NCIS\",\n" +
                "    \"matcher\" : \"NCIS.*\",\n" +
                "    \"ignore\" : [ ],\n" +
                "    \"target\" : \"/downloads/series/NCIS\"\n" +
                "  }, {\n" +
                "    \"name\" : \"Kalle\",\n" +
                "    \"matcher\" : \"Kalle.*\",\n" +
                "    \"ignore\" : [ ],\n" +
                "    \"target\" : \"/downloads/series/Kalle\"\n" +
                "  } ],\n" +
                "  \"movies\" : {\n" +
                "    \"name\" : \"Movies\",\n" +
                "    \"target\" : \"/downloads/movies\",\n" +
                "    \"size-gb\" : 4.2\n" +
                "  },\n" +
                "  \"log-config\" : {\n" +
                "    \"source-files\" : true,\n" +
                "    \"missed-files\" : false,\n" +
                "    \"copied-files\" : true,\n" +
                "    \"debug-file-operations\" : true\n" +
                "  }\n" +
                "}", s);
        LOGGER.debug(s);
    }

    @Test
    public void testReadFile() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        Config c = mapper.readValue(new File(getClass().getClassLoader().getResource("json/config.json").getFile()), Config.class);

        assertEquals(true, c.getLogConfig().isCopiedFiles());
        assertEquals(true, c.getLogConfig().isMissedFiles());
        assertEquals(true, c.getLogConfig().isSourceFiles());
        assertEquals(false, c.getLogConfig().isDebugFileOperations());
        assertEquals(3600, c.getDelay());
        for( String source : c.getSources() ) {
            assertEquals("~/Storage/transmission/download/completed", source);
        }

        for( Series series : c.getSeries() ) {
            if( series.getName().equals("NCIS") ) {
                assertEquals("NCIS", series.getName());
                assertEquals("NCIS.*", series.getMatcher());
                assertEquals(2, series.getIgnore().size());
                assertEquals("Los.*Angeles", series.getIgnore().get(0));
                assertEquals("New.*Orleans", series.getIgnore().get(1));
                assertEquals("/downloads/series/NCIS", series.getTarget());
            } else if ( series.getName().equals("Last Man Standing") ) {
                assertEquals("Last Man Standing", series.getName());
                assertEquals("Last.*Man.*Standing", series.getMatcher());
                assertEquals(0, series.getIgnore().size());
                assertEquals("/downloads/series/Last Man Standing", series.getTarget());
            } else {
                LOGGER.debug(series);
            }
        }

        LOGGER.debug(c);
    }

    @Test
    @Ignore
    public void testSortOriginalFile() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        Config input = mapper.readValue(new File(getClass().getClassLoader().getResource("json/original.json").getFile()), Config.class);

        List<Series> series = input.getSeries();
        for( int i = 0; i < series.size(); i++ ) {
            for( int j = i + 1; j < series.size(); j++ ) {
                if( 0 < series.get(i).getName().compareTo(series.get(j).getName()) ) {
                    Series temp = series.get(i);
                    series.set(i, series.get(j));
                    series.set(j, temp);
                }
            }
        }
        input.setSeries(series);
        LOGGER.debug(mapper.writeValueAsString(input));
    }

}
