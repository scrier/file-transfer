package io.scrier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.scrier.json.Config;
import io.scrier.json.Series;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.kohsuke.args4j.CmdLineException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by scrier on 26/09/16.
 */
public class SystemTest {

    private static final Logger LOGGER = LogManager.getLogger(SystemTest.class);

    Context context;
    HelperMethods helperMethods;

    @Before
    public void setup() {
        helperMethods = new HelperMethods();
        context = Context.getInstance();
    }

    @After
    public void tearDown() {
        helperMethods.cleanup();
        context = null;
    }

    @Test(expected = CommandLineException.class)
    public void testNoCommandLineArgs() throws CommandLineException, CmdLineException {
        Main main = new Main(new String[] {} ).initCommandLine();
    }

    @Test
    public void testOkCommandLine() throws CommandLineException, CmdLineException, IOException {
        File logs = helperMethods.createTempDir("temp", "log");
        File config = helperMethods.createTempDir("temp", "config");
        Main main = new Main(new String[] {"-config",config.getAbsolutePath(),"-logs",logs.getAbsolutePath()} ).initCommandLine();
        assertEquals(logs.getAbsolutePath(), context.getLogDirectory());
        assertEquals(config.getAbsolutePath(), context.getConfigDirectory());
        logs.delete();
        config.delete();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotOkConfig() throws CommandLineException, CmdLineException, IOException {
        Main main = new Main(new String[] {} ).initConfig();
    }

    @Test
    public void testNoFileConfig() throws CommandLineException, CmdLineException, IOException {
        File logs = helperMethods.createTempDir("temp", "log");
        File config = helperMethods.createTempDir("temp", "config");
        Main main = new Main(new String[] {"-config",config.getAbsolutePath(),"-logs",logs.getAbsolutePath()} )
                .initCommandLine().initConfig();
        assertNotNull(context.getConfigLoader().getConfig());
        Config cfg = context.getConfigLoader().getConfig();
        assertEquals(3600, cfg.getDelay());
        assertEquals(1, cfg.getSources().size());
        assertEquals("/data/transmission", cfg.getSources().get(0));
        assertEquals(1, cfg.getSeries().size());
        assertEquals("Foo", cfg.getSeries().get(0).getName());
        assertEquals(".*Foo.*", cfg.getSeries().get(0).getMatcher());
        assertEquals(1, cfg.getSeries().get(0).getIgnore().size());
        assertEquals(".*Bar.*", cfg.getSeries().get(0).getIgnore().get(0));
        assertEquals("/data/targets", cfg.getSeries().get(0).getTarget());
    }

    @Test
    public void testOkConfig() throws CommandLineException, CmdLineException, IOException {
        File logs = helperMethods.createTempDir("temp", "log");
        File config = File.createTempFile("temp", "config");
        Config expected = helperMethods.createDummyConfig();
        helperMethods.writeConfigFile(config, expected);
        Main main = new Main(new String[] {"-config",config.getParent(),"-logs",logs.getAbsolutePath(),
                "-config-file",config.getName()} ).initCommandLine().initConfig();
        assertEquals(expected.toString(), context.getConfigLoader().getConfig().toString());
        LOGGER.debug("Config: " + expected.toString());
    }

    @Test
    public void testLastManStanding() throws IOException, CommandLineException, CmdLineException {
        Series lastManStanding = new Series().setName("Last Man Standing")
                .setMatcher("Last.*Man.*Standing.*E[0-9]{2}")
                .setTarget("/downloads/Last Man Standing")
                .init();
        File logs = helperMethods.createTempDir("temp", "files");
        List<File> files = helperMethods.inputExample(logs);
        assertEquals(17, files.size());
        List<Integer> expectedTrue = Arrays.asList(10, 11);
        for( int i  = 0; i < files.size(); i++ ) {
            if (expectedTrue.contains(i)) {
                LOGGER.debug("Checking " + files.get(i).getName() + " towards true at index " + i + ".");
                assertEquals(files.get(i).getName(), true, lastManStanding.match(files.get(i)));
            } else {
                LOGGER.debug("Checking " + files.get(i).getName() + " towards false at index " + i + ".");
                assertEquals(files.get(i).getName(), false, lastManStanding.match(files.get(i)));
            }
        }
    }

    @Test
    public void testNCIS() throws IOException, CommandLineException, CmdLineException {
        Series lastManStanding = new Series().setName("NCIS")
                .setMatcher("NCIS.*E[0-9]{2}")
                .addIgnore("Los.*Angeles")
                .addIgnore("New.*Orleans")
                .setTarget("/downloads/NCIS")
                .init();
        File logs = helperMethods.createTempDir("temp", "files");
        List<File> files = helperMethods.inputExample(logs);
        assertEquals(17, files.size());
        List<Integer> expectedTrue = Arrays.asList(1,4,9,16);
        for( int i  = 0; i < files.size(); i++ ) {
            if (expectedTrue.contains(i)) {
                LOGGER.debug("Checking true => " + files.get(i).getName() + " towards index " + i + ".");
                assertEquals(files.get(i).getName(), true, lastManStanding.match(files.get(i)));
            } else {
                LOGGER.debug("Checking false => " + files.get(i).getName() + " towards index " + i + ".");
                assertEquals(files.get(i).getName(), false, lastManStanding.match(files.get(i)));
            }
        }
    }

    @Test
    public void testLogOutput() throws CommandLineException, CmdLineException, IOException {
        File logs = helperMethods.createTempDir("temp", "log");
        File config = File.createTempFile("temp", "config");
        File source = helperMethods.createTempDir("source", "file");
        File target = helperMethods.createTempDir("target", "file");
        File fileFound = helperMethods.createTempFile(source, "test.dat");
        File fileNotFound = helperMethods.createTempFile(source, "temp.dat");
        LOGGER.debug("Source dir [" + source.isDirectory() + "]: " + source.getAbsolutePath());
        LOGGER.debug("Target dir [" + target.isDirectory() + "]: " + target.getAbsolutePath());
        LOGGER.debug("File Found: " + fileFound.getAbsolutePath());
        LOGGER.debug("File Not Found: " + fileNotFound.getAbsolutePath());
        Config expected = helperMethods.createDummyConfig(new ArrayList<String>() {{ add(source.getAbsolutePath()); }},
                new ArrayList<Series>() {{
                    add(new Series().setName("test").setMatcher("test.*dat").setTarget(target.getAbsolutePath()));
                }});
        helperMethods.writeConfigFile(config, expected);
        Main main = new Main(new String[] {"-config",config.getParent(),"-logs",logs.getAbsolutePath(),
                "-config-file",config.getName()} ).initCommandLine().initConfig();
    }



}
