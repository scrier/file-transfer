package io.scrier.log;

import io.scrier.Context;
import io.scrier.HelperMethods;
import io.scrier.file.FileHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by scrier on 30/09/16.
 */
public class FilesTreeTest {

    private static final Logger LOGGER = LogManager.getLogger(FilesTreeTest.class);

    Context context;
    HelperMethods helperMethods;
    File logFile;

    @Before
    public void setup() throws IOException {
        helperMethods = new HelperMethods();
        context = Context.getInstance();
        File configDir = helperMethods.createTempDir("write", "config");
        logFile = helperMethods.createTempFile(configDir, "input.txt");
        context.setConfigDirectory(configDir.getAbsolutePath());
        context.setCopiedFile(logFile.getName());
    }

    @After
    public void tearDown() {
        helperMethods.cleanup();
        context = null;
    }

    @Test
    public void testWriteToFile() throws IOException {
        File path = helperMethods.createTempDir("write", "test");
        FilesTree input = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).initFile()
                .add(new FileHolder(helperMethods.createTempFile(path, "1")))
                .add(new FileHolder(helperMethods.createTempFile(path, "2")))
                .add(new FileHolder(helperMethods.createTempFile(path, "3")))
                .add(new FileHolder(helperMethods.createTempFile(path, "4")))
                .add(new FileHolder(helperMethods.createTempFile(path, "5")))
                .add(new FileHolder(helperMethods.createTempFile(path, "6")))
                .write();
        List<FileHolder> expected = input.getFileTree();
        String line;
        try (
                InputStream fis = new FileInputStream(logFile);
                InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
                BufferedReader br = new BufferedReader(isr);
        ) {
            LOGGER.info("Reading: " + logFile.getAbsolutePath());
            while ((line = br.readLine()) != null) {
                LOGGER.info("File: " + line + ", removed: " + expected.remove(new FileHolder(line)));
            }
            br.close();
        }
        assertTrue(expected.isEmpty());
    }

    @Test
    public void testWriteReadToFile() throws IOException {
        File path = helperMethods.createTempDir("write", "test");
        FilesTree input = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).initFile()
                .add(new FileHolder(helperMethods.createTempFile(path, "1")))
                .add(new FileHolder(helperMethods.createTempFile(path, "2")))
                .add(new FileHolder(helperMethods.createTempFile(path, "3")))
                .add(new FileHolder(helperMethods.createTempFile(path, "4")))
                .add(new FileHolder(helperMethods.createTempFile(path, "5")))
                .add(new FileHolder(helperMethods.createTempFile(path, "6")))
                .write();
        FilesTree expected = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).read();
        assertEquals(expected, input);
    }

    @Test
    public void testDuplicates() throws IOException {
        File path = helperMethods.createTempDir("write", "test");
        FileHolder fh1 = new FileHolder(helperMethods.createTempFile(path, "1"));
        FileHolder fh2 = new FileHolder(helperMethods.createTempFile(path, "2"));
        FileHolder fh3 = new FileHolder(helperMethods.createTempFile(path, "3"));
        FileHolder fh4 = new FileHolder(helperMethods.createTempFile(path, "4"));
        FileHolder fh5 = new FileHolder(helperMethods.createTempFile(path, "5"));
        FileHolder fh6 = new FileHolder(helperMethods.createTempFile(path, "6"));
        FilesTree input = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).initFile()
                .add(fh2)
                .add(fh3)
                .add(fh4)
                .add(fh5);
        List<FileHolder> compare = new ArrayList<FileHolder>() {{
            add(fh1);
            add(fh3);
            add(fh5);
            add(fh6);
        }};
        List<FileHolder> duplicate = input.getDuplicateFiles(compare);
        assertEquals(2, duplicate.size());
        assertTrue(duplicate.remove(fh3));
        assertTrue(duplicate.remove(fh5));
        assertTrue(duplicate.isEmpty());
    }

    @Test
    public void testClassExclusive() throws IOException {
        File path = helperMethods.createTempDir("write", "test");
        FileHolder fh1 = new FileHolder(helperMethods.createTempFile(path, "1"));
        FileHolder fh2 = new FileHolder(helperMethods.createTempFile(path, "2"));
        FileHolder fh3 = new FileHolder(helperMethods.createTempFile(path, "3"));
        FileHolder fh4 = new FileHolder(helperMethods.createTempFile(path, "4"));
        FileHolder fh5 = new FileHolder(helperMethods.createTempFile(path, "5"));
        FileHolder fh6 = new FileHolder(helperMethods.createTempFile(path, "6"));
        FilesTree input = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).initFile()
                .add(fh2)
                .add(fh3)
                .add(fh4)
                .add(fh5);
        List<FileHolder> compare = new ArrayList<FileHolder>() {{
            add(fh1);
            add(fh3);
            add(fh5);
            add(fh6);
        }};
        List<FileHolder> classExclusive = input.getClassExclusive(compare);
        assertEquals(2, classExclusive.size());
        assertTrue(classExclusive.remove(fh2));
        assertTrue(classExclusive.remove(fh4));
        assertTrue(classExclusive.isEmpty());
    }

    @Test
    public void testInputExclusive() throws IOException {
        File path = helperMethods.createTempDir("write", "test");
        FileHolder fh1 = new FileHolder(helperMethods.createTempFile(path, "1"));
        FileHolder fh2 = new FileHolder(helperMethods.createTempFile(path, "2"));
        FileHolder fh3 = new FileHolder(helperMethods.createTempFile(path, "3"));
        FileHolder fh4 = new FileHolder(helperMethods.createTempFile(path, "4"));
        FileHolder fh5 = new FileHolder(helperMethods.createTempFile(path, "5"));
        FileHolder fh6 = new FileHolder(helperMethods.createTempFile(path, "6"));
        FilesTree input = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).initFile()
                .add(fh2)
                .add(fh3)
                .add(fh4)
                .add(fh5);
        List<FileHolder> compare = new ArrayList<FileHolder>() {{
            add(fh1);
            add(fh3);
            add(fh5);
            add(fh6);
        }};
        List<FileHolder> inputExclusice = input.getInputExclusive(compare);
        assertEquals(2, inputExclusice.size());
        assertTrue(inputExclusice.remove(fh1));
        assertTrue(inputExclusice.remove(fh6));
        assertTrue(inputExclusice.isEmpty());
    }

    @Test
    public void testImmediateChildren() throws IOException {
        File path = helperMethods.createTempDir("write", "test");
        FileHolder fh1 = new FileHolder(helperMethods.createTempFile(path, "1"));
        FileHolder fh2 = new FileHolder(helperMethods.createTempFile(path, "2"));
        FileHolder fh3 = new FileHolder(helperMethods.createTempFile(path, "3"));
        FileHolder fh4 = new FileHolder(helperMethods.createTempFile(path, "4"));
        FileHolder fh5 = new FileHolder(helperMethods.createTempFile(path, "5"));
        FileHolder fh6 = new FileHolder(helperMethods.createTempFile(path, "6"));
        FilesTree input = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).loadImmediateChildren(path);
        List<FileHolder> inputTree = input.getFileTree();
        assertEquals(6, inputTree.size());;
        assertTrue(inputTree.remove(fh6));
        assertTrue(inputTree.remove(fh5));
        assertTrue(inputTree.remove(fh4));
        assertTrue(inputTree.remove(fh3));
        assertTrue(inputTree.remove(fh2));
        assertTrue(inputTree.remove(fh1));
        assertTrue(inputTree.isEmpty());
    }

}
