package io.scrier.file;

import io.scrier.ConfigLoader;
import io.scrier.Context;
import io.scrier.HelperMethods;
import io.scrier.json.Config;
import io.scrier.json.Movies;
import io.scrier.json.Series;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by scrier on 2016-10-06.
 */
public class FileProducerTest {

    private static final Logger LOGGER = LogManager.getLogger(FileProducerTest.class);

    private final String[] filesHit = { "NCIS.New.Orleans.S03E02.720p.HDTV.X264-DIMENSION.mkv",
            "NCIS.New.Orleans.S03E01.720p.HDTV.X264-DIMENSION.mkv",
            "NCIS.S14E03.720p.HDTV.X264-DIMENSION.mkv",
            "NCIS.S14E02.720p.HDTV.X264-DIMENSION.mkv",
            "NCIS.S14E01.720p.HDTV.X264-DIMENSION.mkv",
            "NCIS.Los.Angeles.S08E01E02.720p.HDTV.X264-DIMENSION.mkv",
            "NCIS.Los.Angeles.S08E03.720p.HDTV.X264-DIMENSION.mkv" };

    private final String[] filesMissed = { "Marvels.Agents.of.S.H.I.E.L.D.S04E02.720p.HDTV.x264-AVS.mkv",
            "Gotham.S03E01.720p.HDTV.X264-DIMENSION.mkv",
            "New.Girl.S06E02.720p.HDTV.x264-AVS.mkv"};

    private HelperMethods helperMethods = new HelperMethods();
    private Context context = Context.getInstance();
    private List<File> toRemove;
    private Config config;
    private ConfigLoader loader = Mockito.mock(ConfigLoader.class);
    private File ncisTarget;
    private File laTarget;
    private File noTarget;
    private File movieTarget;
    private File logDir;
    private File configDir;
    private File sourceDir;

    @Before
    public void setup() throws IOException {
        this.toRemove = new ArrayList<>();
        this.sourceDir = helperMethods.createTempDir("source", "dir");
        this.ncisTarget = helperMethods.createTempDir("ncis", "");
        this.laTarget =  helperMethods.createTempDir("ncis", "la");
        this.noTarget =  helperMethods.createTempDir("ncis", "no");
        this.movieTarget =  helperMethods.createTempDir("movie", "yes");
        this.logDir = helperMethods.createTempDir("log", "dir");
        this.configDir = helperMethods.createTempDir("config", "dir");
        this.config = new Config()
                .addSource(sourceDir.getAbsolutePath())
                .addSeries(new Series()
                        .setName("NCIS")
                        .setMatcher(".*NCIS.*")
                        .addIgnore(".*Los.*Angeles.*")
                        .addIgnore(".*New.*Orleans.*")
                        .setTarget(this.ncisTarget.getAbsolutePath()))
                .addSeries(new Series()
                        .setName("NCIS Los Angeles")
                        .setMatcher(".*NCIS.*Los.*Angeles.*")
                        .setTarget(this.laTarget.getAbsolutePath()))
                .addSeries(new Series().setName("NCIS New Orleans")
                        .setMatcher(".*NCIS.*New.*Orleans.*")
                        .setTarget(this.noTarget.getAbsolutePath()))
                .setMovies(new Movies().setName("Movies")
                        .setSizeGB(0.5)
                        .setTarget(this.movieTarget.getAbsolutePath()));
        Mockito.when(loader.reload()).thenReturn(loader);
        Mockito.when(loader.getConfig()).thenReturn(config);
        context.setConfigLoader(loader);
        context.setDryRyn(true);
        context.setConfigDirectory(this.configDir.getAbsolutePath());
        context.setLogDirectory(this.logDir.getAbsolutePath());
        context.setCopiedFile("copied.txt");
        context.setMissedFile("missed.txt");
    }

    @After
    public void tearDown() {
        this.loader = null;
        this.config = null;
        helperMethods.cleanup();
        for( File f : this.toRemove ) {
            if( f.exists() ) {
                f.delete();
            }
        }
        this.context.getQueue().clear();
    }

    @Test
    public void testNoCopiedFiles() throws IOException {
        helperMethods.createTempFile(sourceDir, filesHit[1]);
        helperMethods.createTempFile(sourceDir, filesHit[3]);
        helperMethods.createTempFile(sourceDir, filesHit[4]);
        FileProducer testObject = new FileProducer();
        testObject.run();
        List<String> expected = new ArrayList<>();
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[1]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[3]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[4]);
        assertEquals(expected.size(), this.context.getQueue().size());
        for( FileHolder holder : this.context.getQueue() ) {
            assertTrue(expected.remove(holder.toString()));
        }
        assertTrue(expected.isEmpty());
        assertTrue(helperMethods.getFileAsList(new File(this.configDir, context.getCopiedFile())).isEmpty());
        assertTrue(helperMethods.getFileAsList(new File(this.configDir, context.getMissedFile())).isEmpty());
    }

    @Test
    public void testNoCopiedFilesMissedItems() throws IOException {
        helperMethods.createTempFile(sourceDir, filesHit[0]);
        helperMethods.createTempFile(sourceDir, filesHit[2]);
        helperMethods.createTempFile(sourceDir, filesHit[5]);
        helperMethods.createTempFile(sourceDir, filesMissed[0]);
        helperMethods.createTempFile(sourceDir, filesMissed[1]);
        FileProducer testObject = new FileProducer();
        testObject.run();
        List<String> expected = new ArrayList<>();
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[0]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[2]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[5]);
        assertEquals(expected.size(), this.context.getQueue().size());
        for( FileHolder holder : this.context.getQueue() ) {
            assertTrue(expected.remove(holder.toString()));
        }
        assertTrue(expected.isEmpty());
        assertTrue(helperMethods.getFileAsList(new File(this.configDir, context.getCopiedFile())).isEmpty());
        List<String> missedItems = helperMethods.getFileAsList(new File(this.configDir, context.getMissedFile()));
        expected.add(sourceDir.getAbsolutePath() + "/" + filesMissed[0]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesMissed[1]);
        assertEquals(expected.size(), missedItems.size());
        expected.removeAll(missedItems);
        assertTrue(expected.isEmpty());
    }

    @Test
    public void testCopiedFiles() throws IOException {
        helperMethods.createTempFile(sourceDir, filesHit[0]);
        helperMethods.createTempFile(sourceDir, filesHit[1]);
        File stored1 = helperMethods.createTempFile(sourceDir, filesHit[2]);
        helperMethods.createTempFile(sourceDir, filesHit[3]);
        File stored2 = helperMethods.createTempFile(sourceDir, filesHit[4]);
        helperMethods.writeListToFile(Arrays.asList(stored1.getAbsolutePath(), stored2.getAbsolutePath()),
                new File(this.configDir, context.getCopiedFile()));
        FileProducer testObject = new FileProducer();
        testObject.run();
        List<String> expected = new ArrayList<>();
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[0]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[1]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[3]);
        assertEquals(expected.size(), this.context.getQueue().size());
        for( FileHolder holder : this.context.getQueue() ) {
            assertTrue(expected.remove(holder.toString()));
        }
        assertTrue(expected.isEmpty());
        assertEquals(2, helperMethods.getFileAsList(new File(this.configDir, context.getCopiedFile())).size());
        assertTrue(helperMethods.getFileAsList(new File(this.configDir, context.getMissedFile())).isEmpty());
    }

    @Test
    public void testCopiedMovie() throws IOException {
        File toCopy = new File(sourceDir, "movie.mkv");
        RandomAccessFile item = new RandomAccessFile(toCopy, "rw");
        item.setLength(1024*1024*1024);
        item.skipBytes(1024*1024*1022);
        item.writeBytes("acdsf");
        item.close();
        FileProducer testObject = new FileProducer();
        testObject.run();
        List<String> expected = new ArrayList<>();
        expected.add(sourceDir.getAbsolutePath() + "/movie.mkv");
        assertEquals(expected.size(), this.context.getQueue().size());
        for( FileHolder holder : this.context.getQueue() ) {
            assertTrue(expected.remove(holder.toString()));
        }
        assertTrue(expected.isEmpty());
        assertTrue(helperMethods.getFileAsList(new File(this.configDir, context.getMissedFile())).isEmpty());
    }

    @Test
    public void testSourceExistingQueue() throws IOException {
        helperMethods.createTempFile(sourceDir, filesHit[0]);
        File exists = helperMethods.createTempFile(sourceDir, filesHit[1]);
        helperMethods.createTempFile(sourceDir, filesHit[3]);
        Optional<Series> inQueue = this.config.getSeriesByName("NCIS New Orleans");
        assertTrue(inQueue.isPresent());
        this.context.getQueue().add(new FileHolder(new FileHolder(exists), inQueue.get()));
        FileProducer testObject = new FileProducer();
        testObject.run();
        List<String> expected = new ArrayList<>();
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[0]);
        expected.add(sourceDir.getAbsolutePath() + "/" + filesHit[3]);
        expected.add(exists.getAbsolutePath());
        assertEquals(expected.size(), this.context.getQueue().size());
        for( FileHolder holder : this.context.getQueue() ) {
            assertTrue(expected.remove(holder.toString()));
        }
        assertTrue(expected.isEmpty());
        assertTrue(helperMethods.getFileAsList(new File(this.configDir, context.getCopiedFile())).isEmpty());
        assertTrue(helperMethods.getFileAsList(new File(this.configDir, context.getMissedFile())).isEmpty());
    }

    @Test
    public void testSeveralSources() throws IOException {
        List<String> hits = Arrays.asList(filesHit);
        List<String> misses = Arrays.asList(filesMissed);
        List<String> items = new ArrayList<>();
        items.addAll(hits);
        items.addAll(misses);
        Random rand = new Random();
        List<String> expectedCopied = new ArrayList<>();
        List<String> expectedMissed = new ArrayList<>();
        List<String> expectedQueue = new ArrayList<>();
        FileProducer testObject = new FileProducer();
        LOGGER.debug(">>>>>>>>>>>>>> There are a total of " + items.size() + " to test on!");
        int maxTries = 100;
        do {
            List<String> toCreate = new ArrayList<>();
            int toAdd = rand.nextInt(items.size()) + 1;
            LOGGER.debug(">>>>>>>>>>>>>> Adding " + toAdd + " to copy!");
            for( int i = 0; i < toAdd; i++ ) {
                while( true ) {
                    int index = rand.nextInt(items.size());
                    if( !toCreate.contains(items.get(index)) ) {
                        toCreate.add(items.get(index));
                        items.remove(index);
                        break;
                    }
                }
            }
//            toCreate.forEach(LOGGER::debug);
            expectedQueue.addAll(toCreate.stream().filter(t -> hits.contains(t)).collect(Collectors.toList()));
            expectedMissed.addAll(toCreate.stream().filter(t -> misses.contains(t)).collect(Collectors.toList()));
            LOGGER.debug(">>>>>>>>>>>>>> We have " + expectedQueue.size() + " to be added to queue!");
            LOGGER.debug(">>>>>>>>>>>>>> We have " + expectedMissed.size() + " to be added to missed!");
            LOGGER.debug(">>>>>>>>>>>>>> We have " + expectedCopied.size() + " to be added to copied!");
            for( String input : toCreate ) {
                helperMethods.createTempFile(sourceDir, input);
            }
            LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> RUN >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            testObject.run();
            LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DONE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            List<String> actualMissed = helperMethods.getFileAsList(new File(this.configDir, context.getMissedFile()));
            assertEquals(actualMissed.size(), expectedMissed.size());
            assertEquals(context.getQueue().size(), expectedQueue.size());
        } while( !items.isEmpty() && maxTries-- > 0);
    }

}
