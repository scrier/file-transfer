package io.scrier.file;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by scrier on 2016-10-06.
 */
public class FileHolderTest {

    @Test
    public void testDuration() {
        FileHolder testObject = new FileHolder("test.json");
        assertEquals("0:00:00.050", testObject.duration(0, 50*1000*1000));
        assertEquals("0:00:36.000", testObject.duration(0, 36L*1000*1000*1000));
        assertEquals("0:12:00.000", testObject.duration(0, 12*60L*1000*1000*1000));
        assertEquals("5:00:00.000", testObject.duration(0, 5*60L*60L*1000*1000*1000));
    }

}
