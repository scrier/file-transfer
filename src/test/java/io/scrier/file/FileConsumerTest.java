package io.scrier.file;

import io.scrier.ConfigLoader;
import io.scrier.Context;
import io.scrier.HelperMethods;
import io.scrier.json.Config;
import io.scrier.json.Movies;
import io.scrier.json.Series;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by scrier on 2016-10-06.
 */
public class FileConsumerTest {

    private HelperMethods helperMethods = new HelperMethods();
    private Context context = Context.getInstance();
    private List<File> files;
    private Config config;
    private ConfigLoader loader = Mockito.mock(ConfigLoader.class);

    @Before
    public void setup() {
        this.files = new ArrayList<>();
        this.config = new Config();
        Mockito.when(loader.reload()).thenReturn(loader);
        Mockito.when(loader.getConfig()).thenReturn(config);
        context.setConfigLoader(loader);
    }

    @After
    public void tearDown() {
        this.loader = null;
        this.config = null;
        helperMethods.cleanup();
        for( File f : this.files ) {
            if( f.exists() ) {
                f.delete();
            }
        }
    }

    @Test
    public void testCopyFile() throws IOException {
        File logs = helperMethods.createTempDir("temp", "log");
        File config = helperMethods.createTempDir("temp", "config");
        File sourceDir = helperMethods.createTempDir("temp", "source");
        File toCopy = new File(sourceDir, "temp.mkv");
        RandomAccessFile item = new RandomAccessFile(toCopy, "rw");
        item.setLength(1024*1024*1024);
        item.skipBytes(1024*1024*1022);
        item.writeBytes("acdsf");
        item.close();
        File targetDir = helperMethods.createTempDir("temp", "target");
        this.files.add(logs);
        this.files.add(config);
        this.files.add(sourceDir);
        this.files.add(toCopy);
        this.files.add(targetDir);
        context.getQueue().add(new FileHolder(new FileHolder(toCopy), new Series().setName("Name").setTarget(targetDir.getAbsolutePath())));
        context.setConfigDirectory(config.getAbsolutePath());
        context.setCopiedFile("copied.txt");
        context.setDryRyn(true);
        FileConsumer testObject = new FileConsumer();
        testObject.run();
        assertEquals(1, targetDir.listFiles().length);
        assertEquals("temp.mkv", targetDir.listFiles()[0].getName());
    }

    @Test
    public void testCopyMovie() throws IOException {
        File logs = helperMethods.createTempDir("temp", "log");
        File config = helperMethods.createTempDir("temp", "config");
        File sourceDir = helperMethods.createTempDir("temp", "source");
        File toCopy = new File(sourceDir, "temp.mkv");
        RandomAccessFile item = new RandomAccessFile(toCopy, "rw");
        item.setLength(1024*1024*1024);
        item.skipBytes(1024*1024*1022);
        item.writeBytes("acdsf");
        item.close();
        File targetDir = helperMethods.createTempDir("temp", "target");
        this.files.add(logs);
        this.files.add(config);
        this.files.add(sourceDir);
        this.files.add(toCopy);
        this.files.add(targetDir);
        context.getQueue().add(new FileHolder(new FileHolder(toCopy), new Movies().setName("Name").setTarget(targetDir.getAbsolutePath())));
        context.setConfigDirectory(config.getAbsolutePath());
        context.setCopiedFile("copied.txt");
        context.setDryRyn(true);
        FileConsumer testObject = new FileConsumer();
        testObject.run();
        assertEquals(1, targetDir.listFiles().length);
        assertEquals("temp.mkv", targetDir.listFiles()[0].getName());

    }

    @Test
    public void testCopyDir() throws IOException {
        File logs = helperMethods.createTempDir("temp", "log");
        File config = helperMethods.createTempDir("temp", "config");
        File sourceDir = helperMethods.createTempDir("temp", "source");
        File toCopy = new File(sourceDir, "temp.mkv");
        RandomAccessFile item = new RandomAccessFile(toCopy, "rw");
        item.setLength(1024*1024*1024);
        item.skipBytes(1024*1024*1022);
        item.writeBytes("acdsf");
        item.close();
        File toCopy2 = new File(sourceDir, "temp2.mkv");
        RandomAccessFile item2 = new RandomAccessFile(toCopy2, "rw");
        item2.setLength(2L*1024*1024*1024);
        item2.skipBytes(2*1024*1024*1022);
        item2.writeBytes("acdsf");
        item2.close();
        File targetDir = helperMethods.createTempDir("temp", "target");
        this.files.add(logs);
        this.files.add(config);
        this.files.add(sourceDir);
        this.files.add(toCopy);
        this.files.add(toCopy2);
        this.files.add(targetDir);
        context.getQueue().add(new FileHolder(new FileHolder(sourceDir), new Series().setName("Name").setTarget(targetDir.getAbsolutePath())));
        context.setConfigDirectory(config.getAbsolutePath());
        context.setCopiedFile("copied.txt");
        context.setDryRyn(true);
        FileConsumer testObject = new FileConsumer();
        testObject.run();
        assertEquals(1, targetDir.listFiles().length);
        assertTrue(targetDir.listFiles()[0].isDirectory());
    }

}
