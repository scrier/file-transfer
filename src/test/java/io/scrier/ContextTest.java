package io.scrier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.scrier.json.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by scrier on 2016-10-07.
 */
public class ContextTest {

    private static final Logger LOGGER = LogManager.getLogger(ContextTest.class);

    private HelperMethods helperMethods = new HelperMethods();

    @Test
    public void testReloadConfig() throws IOException {
        File configPath = helperMethods.createTempDir("config", "path");
        File configFile = helperMethods.createTempFile(configPath, "config.json");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        Config c = mapper.readValue(new File(getClass().getClassLoader().getResource("json/config.json").getFile()), Config.class);
        mapper.writeValue(configFile, c);

        Context testObject = Context.getInstance()
                .setConfigDirectory(configPath.getAbsolutePath())
                .setConfigFile("config.json")
                .loadConfig();

        assertEquals(c, testObject.getConfigLoader().getConfig());
        LOGGER.debug(c.toString());

        c.getLogConfig().setDebugFileOperations(!c.getLogConfig().isDebugFileOperations());
        c.getLogConfig().setCopiedFiles(!c.getLogConfig().isCopiedFiles());
        c.getLogConfig().setMissedFiles(!c.getLogConfig().isMissedFiles());
        c.getLogConfig().setSourceFiles(!c.getLogConfig().isSourceFiles());

        c.getSources().add("another/source");

        mapper.writeValue(configFile, c);

        testObject.reloadConfig();

        assertEquals(c, testObject.getConfigLoader().getConfig());

        LOGGER.debug(c.toString());
    }

}
