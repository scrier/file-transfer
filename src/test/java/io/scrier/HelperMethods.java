package io.scrier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.scrier.json.Config;
import io.scrier.json.Series;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by scrier on 30/09/16.
 */
public class HelperMethods {

    private List<File> cleanup;

    public HelperMethods() {
        this.cleanup = new ArrayList<>();
    }

    public HelperMethods cleanup() {
        for( File f : cleanup ) {
            f.delete();
        }
        return this;
    }

    public Config createDummyConfig() {
        return createDummyConfig(new ArrayList<String>() {{
                                     add("~Storage/transmission/download/completed");
                                     add("Another source");}},
                new ArrayList<Series>() {{
                    add(new Series()
                            .setName("NCIS")
                            .setMatcher("NCIS.*")
                            .setTarget("/downloads/series/NCIS"));
                    add(new Series()
                            .setName("Kalle")
                            .setMatcher("Kalle.*")
                            .setTarget("/downloads/series/Kalle"));
                }});
    }

    public Config createDummyConfig(List<String> sources, List<Series> series) {
        Config result = new Config();
        for( String source : sources ) {
            result.addSource(source);
        }
        for( Series serie : series ) {
            result.addSeries( serie );
        }
        return result;
    }

    public void writeConfigFile(File configFile, Config config) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.writeValue(configFile, config);
    }

    public final File createTempDir(String prefix, String postfix) throws IOException {
        File file = File.createTempFile(prefix, postfix);
        file.delete();
        file.mkdir();
        this.cleanup.add(file);
        return file;
    }

    public final File createTempDir(File path, String folder) throws IOException {
        File file =  new File(path.getAbsolutePath() + File.separator + folder);
        file.createNewFile();
        file.delete();
        file.mkdir();
        this.cleanup.add(file);
        return file;
    }

    public final File createTempFile(File path, String filename) throws IOException {
        File file = new File(path.getAbsolutePath() + File.separator + filename);
        file.createNewFile();
        this.cleanup.add(file);
        return file;
    }

    public final void writeListToFile(List<String> strings, File output) throws IOException {
        FileWriter writer = new FileWriter(output);
        for(String str: strings) {
            writer.write(str);
            writer.write("\r\n");
        }
        writer.flush();
        writer.close();
    }

    public final List<String> getFileAsList(File input) throws FileNotFoundException {
        Scanner s = new Scanner(input);
        ArrayList<String> list = new ArrayList<String>();
        while (s.hasNext()){
            list.add(s.next());
        }
        s.close();
        return list;
    }

    public List<File> inputExample(File path) throws IOException {
        List<File> result = new ArrayList<>();
        result.add(createTempFile(path, "NCIS: New Orleans S03E01 1080p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "NCIS S14E02 1080p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "NCIS: New Orleans S03E02 1080p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "NCIS: New Orleans S03E02 720p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "NCIS S14E02 720p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "NCIS: Los Angeles S08E01-02 1080p WEB-DL DD5.1 H.264.mkv"));
        result.add(createTempFile(path, "NCIS: Los Angeles S08E01-02 720p WEB-DL DD5.1 H.264.mkv"));
        result.add(createTempFile(path, "NCIS: Los Angeles S08E01-E02 720p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "NCIS: Los Angeles S08E01-E02 1080p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "NCIS S14E01 720p HDTV x264-DIMENSION.mkv"));
        result.add(createTempFile(path, "Last Man Standing S06E02 1080p WEB-DL DD5.1 H.264-NTb.mkv"));
        result.add(createTempFile(path, "Last Man Standing S06E01 720p WEB-DL DD5.1 H.264-NTb"));
        result.add(createTempFile(path, "Last Man Standing S05 720p HDTV x264-Scene.mkv"));
        result.add(createTempFile(path, "Last Man Standing S04 720p WEB-DL DD5.1 H.264-pcsyndicate.mkv"));
        result.add(createTempFile(path, "The Last Man on Earth S03E01 720p HDTV x264-AVS"));
        result.add(createTempFile(path, "NCIS S13 720p WEB-DL DD5.1 H.264"));
        result.add(createTempDir(path, "NCIS S13E02 720p HDTV x264-DIMENSION"));
        return result;
    }

}
