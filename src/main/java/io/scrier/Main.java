package io.scrier;

import com.google.common.annotations.VisibleForTesting;
import io.scrier.file.FileConsumer;
import io.scrier.file.FileProducer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.plugins.PluginVisitorStrategy;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by scrier on 24/09/16.
 */
public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    private CmdLineParser cmdLineParser;
    private Context context = Context.getInstance();
    private final String[] args;
    private final ExecutorService executorService;
    private FileProducer fileProducer;
    private FileConsumer fileConsumer;

    public static void main( String[] args ) {
        try {
            System.out.println("Starting main");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            LOGGER.info(">>>>>>>>>>>>>>>>>>>>>> Starting application Version 2.0 at " + dateFormat.format(new Date()) +
                    " <<<<<<<<<<<<<<<<<<<<<<");
            new Main(args).init();
        } catch (CmdLineException e) {
            LOGGER.fatal("CmdLineException: ", e);
            System.exit(1);
        } catch (CommandLineException e) {
            LOGGER.fatal("CommandLineException: ", e);
            System.exit(1);
        }
        System.exit(0);
    }

    public Main(String[] args) throws CmdLineException, CommandLineException {
        this.args = args;
        this.executorService = Executors.newFixedThreadPool(2);
    }

    public Main initCommandLine() throws CmdLineException, CommandLineException {
        CommandLineArguments commandLineArguments = new CommandLineArguments();
        cmdLineParser = new CmdLineParser(commandLineArguments);
        cmdLineParser.parseArgument(this.args);
        commandLineArguments.validate();
        context.init(commandLineArguments);
        return this;
    }

    public Main initConfig() {
        context.loadConfig();
        return this;
    }

    public Main run() {
        try {
            context.setRunning(true);
            Collection<Future<?>> futures = new LinkedList<Future<?>>();
            LOGGER.info("Starting FileProducer.");
            fileProducer = new FileProducer();
            futures.add(this.executorService.submit(fileProducer));
            LOGGER.info("Starting FileConsumer");
            fileConsumer = new FileConsumer();
            futures.add(this.executorService.submit(new FileConsumer()));
            for (Future<?> future:futures) {
                future.get();
            }
            context.setRunning(false);
            LOGGER.info("Terminating services");
        } catch (IOException e) {
            LOGGER.error("IOException", e);
            System.exit(1);
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException", e);
            System.exit(1);
        } catch (ExecutionException e) {
            LOGGER.error("ExecutionException", e);
            System.exit(1);
        }
        return this;
    }

    public Main init() throws CommandLineException, CmdLineException {
        return initCommandLine().initConfig().run();
    }

}
