package io.scrier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.scrier.file.FileHolder;
import io.scrier.file.FileOperations;
import io.scrier.json.Config;
import io.scrier.json.Series;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by andreas on 29/09/16
 */
public class Context {

    private static final Logger LOGGER = LogManager.getLogger(Context.class);

    private String configDirectory;
    private String logDirectory;
    private String configFile;
    private String copiedFile;
    private String missedFile;
    private boolean dryRyn;
    private boolean running;
    private BlockingQueue<FileHolder> queue;
    private ConfigLoader configLoader;
    private final Lock copiedFileWrite;

    private static class Holder {
        final static Context INSTANCE = new Context();
    }

    public static Context getInstance() {
        return Holder.INSTANCE;
    }

    private Context() {
        setConfigDirectory(null);
        setLogDirectory(null);
        setConfigFile(null);
        setDryRyn(false);
        setRunning(true);
        setQueue(new LinkedBlockingQueue<>());
        this.copiedFileWrite = new ReentrantLock();
    }

    public Context init(CommandLineArguments arguments) {
        setConfigDirectory(arguments.getConfigDirectory());
        setLogDirectory(arguments.getLogDirectory());
        setConfigFile(arguments.getConfigFile());
        setCopiedFile(arguments.getCopiedFile());
        setMissedFile(arguments.getMissedFile());
        return this;
    }

    public Context loadConfig() {
        if( null != getConfigDirectory() &&
                FileOperations.exists(getConfigDirectory()) &&
                !FileOperations.exists(new File(getConfigDirectory(), getConfigFile())) ) {
            LOGGER.info("Creating default config file!");
            Config config = new Config()
                    .setDelay(3600)
                    .addSource("/data/transmission")
                    .addSeries(new Series()
                            .setName("Foo")
                            .setMatcher(".*Foo.*")
                            .addIgnore(".*Bar.*")
                            .setTarget("/data/targets"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            try {
                new File(getConfigDirectory()).mkdir();
                mapper.writeValue(new File(getConfigDirectory(), getConfigFile()), config);
            } catch (IOException e) {
                LOGGER.error("IOException when writing default config file!", e);
                System.exit(1);
            }
        }
        configLoader = new ConfigLoader(Paths.get(getConfigDirectory(),
                getConfigFile())).init();
        return this;
    }

    public Context reloadConfig() {
        this.configLoader.reload();
        return this;
    }

    public String getConfigDirectory() {
        return configDirectory;
    }

    public Context setConfigDirectory(String configDirectory) {
        this.configDirectory = configDirectory;
        return this;
    }

    public String getLogDirectory() {
        return logDirectory;
    }

    public Context setLogDirectory(String logDirectory) {
        this.logDirectory = logDirectory;
        return this;
    }

    public String getConfigFile() {
        return configFile;
    }

    public Context setConfigFile(String configFile) {
        this.configFile = configFile;
        return this;
    }

    public String getCopiedFile() {
        return copiedFile;
    }

    public Context setCopiedFile(String copiedFile) {
        this.copiedFile = copiedFile;
        return this;
    }

    public String getMissedFile() {
        return missedFile;
    }

    public Context setMissedFile(String missedFile) {
        this.missedFile = missedFile;
        return this;
    }

    public boolean isDryRyn() {
        return dryRyn;
    }

    public Context setDryRyn(boolean dryRyn) {
        this.dryRyn = dryRyn;
        return this;
    }

    public boolean isRunning() {
        return running;
    }

    public Context setRunning(boolean running) {
        this.running = running;
        return this;
    }

    public BlockingQueue<FileHolder> getQueue() {
        return queue;
    }

    public Context setQueue(BlockingQueue<FileHolder> queue) {
        this.queue = queue;
        return this;
    }

    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    public Context setConfigLoader(ConfigLoader configLoader) {
        this.configLoader = configLoader;
        return this;
    }

    public Lock getCopiedFileWrite() {
        return copiedFileWrite;
    }

}
