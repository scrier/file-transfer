package io.scrier.file;

import io.scrier.Context;
import io.scrier.json.Config;
import io.scrier.json.Movies;
import io.scrier.json.Series;
import io.scrier.log.FilesTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;

/**
 * Created by andreas on 29/09/16
 */
public class FileProducer implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(FileProducer.class);

    private final BlockingQueue blockingQueue;
    private final Context context = Context.getInstance();
    private final FilesTree copiedFiles;
    private final FilesTree filesToCopy;
    private final Lock copiedFileWrite;
    private Config config;
    private List<FileHolder> filesCopiedNotRemovedList;
    private List<FileHolder> filesToCopyList;

    public FileProducer() throws IOException {
        this.blockingQueue = context.getQueue();
        this.copiedFileWrite = context.getCopiedFileWrite();
        this.copiedFiles = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).initFile();
        this.filesToCopy = new FilesTree(context.getConfigDirectory(), context.getCopiedFile());
        this.filesToCopyList = new ArrayList<>();
        this.filesCopiedNotRemovedList = new ArrayList<>();
    }

    @Override
    public void run() {
        LOGGER.info("Starting FileProducer");
        while( context.isRunning() ) {
            this.config = context.reloadConfig().getConfigLoader().getConfig();
            populateFilesToCopy();
            queueFilesToCopy();
            try {
                LOGGER.debug("Sleeping for " + formatDelay(context.getConfigLoader().getConfig().getDelay()) + ".");
                if( context.isDryRyn() ) {
                    return;
                }
                Thread.sleep(context.getConfigLoader().getConfig().getDelay() * 1000);
            } catch (InterruptedException e) {
                LOGGER.error("Received InterruptedException: ", e);
            }
        }
    }

    private void queueFilesToCopy() {
        List<FileHolder> unMatchedFiles = new ArrayList<>();
        for ( FileHolder fileHolder : this.filesToCopyList ) {
            Optional<Series> matchingSerie = this.config.getMatchingSerie(fileHolder);
            if( matchingSerie.isPresent() ) {
                LOGGER.info("Adding series " + fileHolder.getSource().getName() + " to queue.");
                blockingQueue.add(new FileHolder(fileHolder, matchingSerie.get()));
            } else if (this.config.isMatchingMovie(fileHolder) ) {
                LOGGER.info("Adding movie " + fileHolder.getSource().getName() + " to queue.");
                blockingQueue.add(new FileHolder(fileHolder, this.config.getMovies()));
            } else {
                unMatchedFiles.add(fileHolder);
                LOGGER.info("Unmatched file " + fileHolder.getSource().getName() + " has size: " + fileHolder.size() + ".");
            }
        }
        try {
            FilesTree missedFiles = new FilesTree(context.getConfigDirectory(), context.getMissedFile()).initFile()
                    .read(this.config.getLogConfig().isMissedFiles());
            List<FileHolder> duplicateFiles = missedFiles.getDuplicateFiles(unMatchedFiles);
            List<FileHolder> missingFiles = missedFiles.getInputExclusive(unMatchedFiles);
            missedFiles.setFileTree(missingFiles).add(duplicateFiles).write(this.config.getLogConfig().isMissedFiles());
        } catch( IOException e ) {
            LOGGER.fatal("IOException: " + e);
        }
    }

    private void populateFilesToCopy() {
        LOGGER.trace("populateFilesToCopy()");
        filesCopiedNotRemovedList.clear();
        filesToCopyList.clear();
        this.copiedFileWrite.lock();
        if( this.config.getLogConfig().isDebugFileOperations() )
            LOGGER.debug("Locked copied file for reading.");
        try {
            this.copiedFiles.read(this.config.getLogConfig().isCopiedFiles());
            if( this.config.getLogConfig().isDebugFileOperations() )
                LOGGER.debug("Copied Files: " + this.copiedFiles.toString() + ".");
            this.filesToCopy.loadFromConfig(context.getConfigLoader().getConfig());
            if( this.config.getLogConfig().isDebugFileOperations() )
                LOGGER.debug("Source Files: " + this.filesToCopy.toString() + ".");
            this.filesCopiedNotRemovedList = this.copiedFiles.getDuplicateFiles(this.filesToCopy);
            if( this.config.getLogConfig().isDebugFileOperations() )
                LOGGER.debug("Items removed from Copied Files: " + this.copiedFiles.getClassExclusive(this.filesToCopy) + ".");
            this.filesToCopyList = this.filesToCopy.getClassExclusive(this.copiedFiles);
            if( this.config.getLogConfig().isDebugFileOperations() )
                LOGGER.debug("Updating Copied Files with: " + this.filesCopiedNotRemovedList + ".");
            this.copiedFiles.setFileTree(filesCopiedNotRemovedList).write(this.config.getLogConfig().isCopiedFiles());
        } catch (IOException e) {
            LOGGER.fatal("IOException: " + e);
        } finally {
            if( this.config.getLogConfig().isDebugFileOperations() )
                LOGGER.debug("Released copied file.");
            this.copiedFileWrite.unlock();
        }
        if( this.config.getLogConfig().isDebugFileOperations() )
            LOGGER.debug("Got " + filesToCopyList.size() + " from sources.");
        filesToCopyList.removeAll(this.blockingQueue);
        LOGGER.info("Removed " + this.blockingQueue.size() + " files to copy from queue.");
    }

    private String formatDelay(int seconds) {
        int second = seconds % 60;
        int minutes = seconds % 3600 / 60;
        int hours = seconds / 3600;
        StringBuilder builder = new StringBuilder();
        if( hours > 0 ) {
            builder.append(" " + hours + " hour" + (hours == 1 ? "" : "s"));
        }
        if( minutes > 0 ) {
            builder.append(" " + minutes + " minute" + (minutes == 1 ? "" : "s"));
        }
        if( second > 0 ) {
            builder.append(" " + second + " second" + (second == 1 ? "" : "s"));
        }
        return builder.toString().trim();
    }

}
