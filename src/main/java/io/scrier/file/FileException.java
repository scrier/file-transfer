package io.scrier.file;

/**
 * Created by scrier on 02/10/16.
 */
public class FileException extends Exception {

    FileException(String message) {
        super(message);
    }

}
