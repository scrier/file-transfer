package io.scrier.file;

import com.google.common.base.Joiner;
import io.scrier.json.Movies;
import io.scrier.json.Series;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;

import static java.nio.file.StandardCopyOption.*;

/**
 * Created by scrier on 30/09/16.
 */
public class FileHolder {

    private static final Logger LOGGER = LogManager.getLogger(FileHolder.class);

    private final File source;
    private File target;
    private String name;
    private boolean movie;

    public FileHolder(File file) {
        LOGGER.trace("FileHolder(" + file + ")");
        this.source = file;
        this.target = null;
        this.name = null;
        this.movie = false;
    }

    public FileHolder(String file) {
        this(new File(file));
        LOGGER.trace("FileHolder(" + file + ")");
    }

    public FileHolder(FileHolder other, Series series) {
        LOGGER.trace("FileHolder(" + other + ", " + series + ")");
        this.source = other.source;
        this.name = series.getName();
        this.target = new File(series.getTarget());
        this.movie = false;
    }

    public FileHolder(FileHolder other, Movies movies) {
        LOGGER.trace("FileHolder(" + other + ", " + movies + ")");
        this.source = other.source;
        this.name = movies.getName();
        this.target = new File(movies.getTarget());
        this.movie = true;
    }

    public File getSource() {
        LOGGER.trace("getSource()");
        return this.source;
    }

    public File getTarget() {
        return this.target;
    }

    public FileHolder prepareForCopy() throws FileException {
        if( isMovie() && this.source.isFile() ) {
            this.target = new File(this.target, this.source.getName().replace(" ", "."));
            this.target.mkdirs();
            LOGGER.debug("Creating folder {} for the movie", this.target.getAbsolutePath());
        }
        return this;
    }

    public FileHolder verifyForCopy() throws FileException {
        if( !this.source.exists() ) {
            throw new FileException("Source " + this.source.getAbsolutePath() + " doesn't exist!");
        } else if ( !this.target.exists() ) {
            throw new FileException("Target " + this.target.getAbsolutePath() + " doesn't exist!");
        } else if ( !this.target.isDirectory() ) {
            throw new FileException("Target " + this.target.getAbsolutePath() + " is not a directory!");
        }
        return this;
    }

    public FileHolder copy() throws IOException, InterruptedException {
        String[] args = new String[this.source.isDirectory() ? 4 : 3];
        int index = 0;
        args[index++] = "cp";
        if( this.source.isDirectory()) {
            args[index++] = "-r";
        }
        args[index++] = this.source.getAbsolutePath();
        args[index++] = this.target.getAbsolutePath();
        LOGGER.debug("Executing command \"" + Joiner.on(" ").join(args) + "\".");
        long startTime = System.nanoTime();
        Process copyProcess = Runtime.getRuntime().exec(args);
        copyProcess.waitFor();
        long endTime = System.nanoTime();
        LOGGER.debug("Copying " + this.source.getName() + " took " +  duration(startTime, endTime) + ", and size was " + size(this.source) +".");
        return this;
    }

    public String size() {
        return size(this.source);
    }

    public String size(File file) {
        double length = recursiveLength(file);
        return new DecimalFormat("#.000").format(length /1024/1024/1024) + "GB";
    }

    public double recursiveLength(File file) {
        double result = 0.0;
        if( file.isFile() ) {
            result += file.length();
        } else {
            for( File f : file.listFiles() ) {
                result += recursiveLength(f);
            }
        }
        return result;
    }

    public String duration(long startTime, long endTime) {
        LOGGER.trace("duration(" + startTime + ", " + endTime + ")");
        long duration = (endTime - startTime);
        double total = (double)duration / 1000000000.0;
        int milliseconds = (int)(total * 1000) % 1000;
        int seconds = (int)Math.floor(total % 60);
        int minutes = (int)Math.floor((total / 60) % 60);
        int hours = (int)Math.floor(total / 3600);
        return new StringBuilder().append(hours).append(":").append(String.format("%02d", minutes)).append(":")
                .append(String.format("%02d", seconds)).append(String.format(".%03d", milliseconds)).toString();
    }

    public boolean isMovie() {
        return this.movie;
    }

    @Override
    public boolean equals(Object other) {
        if( null == other ) {
            return false;
        } else if ( !(other instanceof FileHolder) ) {
            return false;
        } else {
            final FileHolder fileHolder = (FileHolder)other;
            if( this.source == null || fileHolder.source == null ) {
                return false;
            } else if( this.source.equals(fileHolder.source) ) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public String toString() {
        return this.source.getAbsolutePath();
    }

}
