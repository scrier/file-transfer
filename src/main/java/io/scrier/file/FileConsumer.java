package io.scrier.file;

import io.scrier.Context;
import io.scrier.json.Config;
import io.scrier.log.FilesTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;

/**
 * Created by andreas on 29/09/16
 */
public class FileConsumer implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(FileConsumer.class);

    private final BlockingQueue<FileHolder> blockingQueue;
    private final Context context = Context.getInstance();
    private final Lock copiedFileWrite;
    private final FilesTree filesTreeFile;
    private final List<FileHolder> filesCopied;

    public FileConsumer() throws IOException {
        this.blockingQueue = context.getQueue();
        this.copiedFileWrite = context.getCopiedFileWrite();
        this.filesTreeFile = new FilesTree(context.getConfigDirectory(), context.getCopiedFile()).initFile();
        this.filesCopied = new ArrayList<>();
    }

    @Override
    public void run() {
        LOGGER.info("Starting FileConsumer");
        while( context.isRunning() ) {
            try {
                FileHolder fileHolder = blockingQueue.take();
                Config config = context.reloadConfig().getConfigLoader().getConfig();
                try {
                    if( config.getLogConfig().isDebugFileOperations() )
                        LOGGER.debug("Copying " + fileHolder.toString());
                    fileHolder.prepareForCopy().verifyForCopy().copy();
                    if( this.copiedFileWrite.tryLock() ) {
                        try {
                            this.filesCopied.add(fileHolder);
                            this.filesTreeFile.read(config.getLogConfig().isCopiedFiles());
                            this.filesTreeFile.add(this.filesCopied).write(config.getLogConfig().isCopiedFiles());
                            this.filesCopied.clear();
                        } finally {
                            this.copiedFileWrite.unlock();
                        }
                    } else {
                        if( config.getLogConfig().isDebugFileOperations() )
                            LOGGER.debug("Queue for next iteration.");
                        this.filesCopied.add(fileHolder);
                    }
                    LOGGER.info("Copied file: " + fileHolder.toString() + " to " +
                            fileHolder.getTarget().getAbsolutePath() + ".");
                } catch (IOException e) {
                    LOGGER.error("IOException on FileConsumer::run.", e);
                } catch (FileException e) {
                    LOGGER.error("FileException on FileConsumer::run.", e);
                }
            } catch (InterruptedException e) {
                LOGGER.fatal("InterruptedException on FileConsumer::run.", e);
            }
            if( context.isDryRyn() ) {
                return;
            }
        }
    }
}
