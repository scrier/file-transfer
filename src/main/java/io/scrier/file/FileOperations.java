package io.scrier.file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by scrier on 24/09/16.
 */
public class FileOperations {


    public static final boolean exists(File file) {
        return file.exists();
    }

    public static final boolean exists(String file) {
        return exists(new File(file));
    }

}
