package io.scrier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.scrier.json.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by scrier on 26/09/16.
 */
public class ConfigLoader {

    private static final Logger LOGGER = LogManager.getLogger(ConfigLoader.class);

    private final Path path;
    private Config config;

    public ConfigLoader(Path path) {
        this.path = path;
        this.config = null;
    }

    public ConfigLoader init() {
        if( this.path.toFile().isDirectory() ) {
            this.path.forEach(file -> {
                if (Files.isRegularFile(file)) {
                    if (file.toString().toLowerCase().endsWith(".json")) {
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
                        try {
                            this.config = mapper.readValue(file.toFile(), Config.class);
                        } catch (IOException e) {
                            LOGGER.fatal("IOException when loading config file: ", e);
                        }
                    }
                }
            });
        } else { // is a file
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            try {
                this.config = mapper.readValue(this.path.toFile(), Config.class);
            } catch (IOException e) {
                LOGGER.fatal("IOException when loading config file: ", e);
            }
        }
        if( null == this.config || true != this.config.validate() ) {
            throw new IllegalArgumentException("Invalid config provided in " + this.path.toString());
        }
        return this;
    }

    public ConfigLoader reload() {
        return init();
    }

    public Config getConfig() {
        return this.config;
    }

}
