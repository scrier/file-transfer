package io.scrier;

/**
 * Created by scrier on 24/09/16.
 */
public class CommandLineException extends Exception {

    public CommandLineException(String exception) {
        super(exception);
    }

}
