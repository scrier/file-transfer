package io.scrier.log;

import com.google.common.base.MoreObjects;
import io.scrier.file.FileHolder;
import io.scrier.json.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by scrier on 30/09/16.
 */
public class FilesTree {

    private static final Logger LOGGER = LogManager.getLogger(FilesTree.class);

    private final File fileTreeLog;
    private final List<FileHolder> fileTree;

    public FilesTree(String path, String file) {
        LOGGER.trace("FilesTree()");
        this.fileTreeLog = new File(path, file);
        this.fileTree = new ArrayList<>();
    }

    public FilesTree initFile() throws IOException {
        LOGGER.trace("initFile()");
        if( !this.fileTreeLog.exists() ) {
            this.fileTreeLog.createNewFile();
        }
        return this;
    }

    public FilesTree append(FilesTree other) {
        LOGGER.trace("append(" + other + ")");
        this.fileTree.addAll(other.fileTree);
        return this;
    }

    public FilesTree appendImmediateChildren(File path, boolean debug) {
        LOGGER.trace("appendImmediateChildren(" + path + ")");
        if( !path.isDirectory() ) {
            LOGGER.error(path.getAbsolutePath() + " is not a path.");
        } else {
            LOGGER.info("Appending " + path.listFiles().length + " items in " + path.getAbsolutePath() + ".");
            for( File file : path.listFiles() ) {
                FileHolder holder = new FileHolder(file);
                if( debug )
                    LOGGER.debug("Adding " + holder.toString() + ".");
                this.fileTree.add(holder);
            }
        }
        return this;
    }

    public FilesTree loadImmediateChildren(File path) {
        LOGGER.trace("loadImmediateChildren(" + path + ")");
        if( !path.isDirectory() ) {
            LOGGER.error(path.getAbsolutePath() + " is not a path.");
        } else {
            if( !this.fileTree.isEmpty()) {
                LOGGER.debug("Removing " + this.fileTree.size() + " items from cache.");
            }
            for( File file : path.listFiles() ) {
                this.fileTree.add(new FileHolder(file));
            }
            LOGGER.info("Found " + this.fileTree.size() + " items in " + path.getAbsolutePath() + ".");
        }
        return this;
    }

    public FilesTree loadFromConfig(Config config) {
        LOGGER.trace("loadImmediateChildren(" + config + ")");
        if( !this.fileTree.isEmpty()) {
            LOGGER.debug("Removing " + this.fileTree.size() + " items from cache.");
            this.fileTree.clear();
        }
        LOGGER.info("Have " + config.getSources().size() + " sources to check!");
        for( String path : config.getSources() ) {
            appendImmediateChildren(new File(path), config.getLogConfig().isSourceFiles());
        }
        LOGGER.debug("Added " + this.fileTree.size() + " items from config!");
        return this;
    }

    public FilesTree read() throws IOException {
        LOGGER.trace("read()");
        return read(false);
    }

    public FilesTree read(boolean debug) throws IOException {
        LOGGER.trace("read()");
        if( !this.fileTree.isEmpty()) {
            if ( debug ) LOGGER.debug("Removing " + this.fileTree.size() + " items from cache.");
        }
        this.fileTree.clear();
        String line;
        try (
                InputStream fis = new FileInputStream(this.fileTreeLog);
                InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
                BufferedReader br = new BufferedReader(isr);
        ) {
            while ((line = br.readLine()) != null) {
                FileHolder holder = new FileHolder(line);
                if( debug )
                    LOGGER.debug("Adding " + holder.toString() + ".");
                this.fileTree.add(holder);
            }
            br.close();
            LOGGER.info("Read " + this.fileTree.size() + " items from " + this.fileTreeLog.getName() + ".");
        }
        return this;
    }

    public FilesTree write() throws IOException {
        LOGGER.trace("write()");
        return write(true);
    }

    public FilesTree write(boolean debug) throws IOException {
        LOGGER.trace("write(" + debug + ")");
        LOGGER.info("Writing " + this.fileTree.size() + " items from cache to " + this.fileTreeLog.getAbsolutePath());
        try (
                PrintWriter writer = new PrintWriter(this.fileTreeLog, "UTF-8");
        ) {
            for(FileHolder fileHolder : this.fileTree) {
                if( debug ) {
                    LOGGER.debug("Writing: " + fileHolder.toString());
                }
                writer.println(fileHolder.toString());
            }
            writer.flush();
            writer.close();
        }
        return this;
    }

    public List<FileHolder> getDuplicateFiles(FilesTree other) {
        LOGGER.trace("getDuplicateFiles(" + other + ")");
        return getDuplicateFiles(other.getFileTree());
    }

    public List<FileHolder> getDuplicateFiles(List<FileHolder> files) {
        LOGGER.trace("getDuplicateFiles(" + files + ")");
        List<FileHolder> result = new ArrayList<>(this.fileTree);
        result.retainAll(files);
        return result;
    }

    public List<FileHolder> getClassExclusive(FilesTree other) {
        LOGGER.trace("getClassExclusive(" + other + ")");
        return getClassExclusive(other.getFileTree());
    }

    public List<FileHolder> getClassExclusive(List<FileHolder> files) {
        LOGGER.trace("getClassExclusive(" + files + ")");
        List<FileHolder> result = new ArrayList<>(this.fileTree);
        result.removeAll(files);
        return result;
    }

    public List<FileHolder> getInputExclusive(FilesTree other) {
        LOGGER.trace("getInputExclusive(" + other + ")");
        return getInputExclusive(other.getFileTree());
    }

    public List<FileHolder> getInputExclusive(List<FileHolder> files) {
        LOGGER.trace("getInputExclusive(" + files + ")");
        List<FileHolder> result = new ArrayList<>(files);
        result.removeAll(this.fileTree);
        return result;
    }

    public List<FileHolder> getFileTree() {
        LOGGER.trace("getFilesCopied()");
        return this.fileTree;
    }

    public FilesTree setFileTree(List<FileHolder> newList) {
        LOGGER.trace("setFilesCopied(" + newList + ")");
        this.fileTree.clear();
        this.fileTree.addAll(newList);
        return this;
    }

    public FilesTree add(FileHolder fileHolder) {
        LOGGER.trace("add(" + fileHolder + ")");
        this.fileTree.add(fileHolder);
        return this;
    }

    public FilesTree add(List<FileHolder> append) {
        LOGGER.trace("add(" + append + ")");
        this.fileTree.addAll(append);
        return this;
    }

    @Override
    public boolean equals(Object other) {
        if( null == other ) {
            return false;
        } else if ( !(other instanceof FilesTree) ) {
            return false;
        } else {
            final FilesTree filesTree = (FilesTree)other;
            if( this.fileTree == null || filesTree.fileTree == null ) {
                return false;
            } else if( this.fileTree.equals(filesTree.fileTree) ) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(FilesTree.class)
                .add("fileTreeLog", this.fileTreeLog)
                .add("fileTree", this.fileTree)
                .toString();
    }

}
