package io.scrier;

import io.scrier.file.FileOperations;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.FileOptionHandler;

/**
 * Created by scrier on 24/09/16.
 */
public class CommandLineArguments {

    @Option(name="-config",usage="Sets where the configuration directory file resides.")
    private String configDirectory;

    @Option(name="-logs",usage="Points to the directory where the logs will reside.")
    private String logDirectory;

    @Option(name="-config-file",usage="Sets the name of the config file if not default.")
    private String configFile="config.json";

    @Option(name="-copied-file",usage="Sets the name of the copied file if not default.")
    private String copiedFile="copied.txt";

    @Option(name="-missed-file",usage="Sets the name of the missed file if not default.")
    private String missedFile="missed.txt";

    public String getLogDirectory() {
        return this.logDirectory;
    }

    public String getConfigDirectory() {
        return this.configDirectory;
    }

    public String getConfigFile() {
        return this.configFile;
    }

    public String getCopiedFile() { return this.copiedFile; }

    public String getMissedFile() { return this.missedFile; }

    public void validate() throws CommandLineException {
        validate(this.configDirectory, "Config Directory");
        validate(this.logDirectory, "Log Directory");
    }

    private void validate(String directory, String description) throws CommandLineException {
        if( null == directory ) {
            throw new CommandLineException("No config value set for " + description + ".");
        } else if ( directory.isEmpty() ) {
            throw new CommandLineException("No config value set for " + description + ".");
        } else if( true != FileOperations.exists(directory)) {
            throw new CommandLineException("Missing directory " + description + " defined for application.");
        }
    }

}
