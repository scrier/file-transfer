package io.scrier.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import io.scrier.file.FileHolder;

import java.util.Optional;

/**
 * Created by scrier on 2016-10-08.
 */
public class Movies {

    private String name;
    @JsonProperty("size-gb")
    private Double sizeGB;
    private String target;

    public Movies() {
        this.name = "";
        this.sizeGB = -1.0;
        this.target = "";
    }

    public String getName() {
        return name;
    }

    public Movies setName(String name) {
        this.name = name;
        return this;
    }

    public Double getSizeGB() {
        return sizeGB;
    }

    public Movies setSizeGB(Double sizeGB) {
        this.sizeGB = sizeGB;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public Movies setTarget(String target) {
        this.target = target;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(Movies.class)
                .add("name", this.name)
                .add("sizeGB", this.sizeGB)
                .add("target", this.target)
                .toString();
    }

}
