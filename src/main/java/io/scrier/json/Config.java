package io.scrier.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import io.scrier.file.FileHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by scrier on 25/09/16.
 */
public class Config {

    private static final Logger LOGGER = LogManager.getLogger(Config.class);

    private int delay;
    @JsonProperty("log-config")
    private LogConfig logConfig;
    private List<String> sources;
    private List<Series> series;
    private Movies movies;

    public Config() {
        LOGGER.traceEntry();
        this.logConfig = new LogConfig();
        this.delay = 1800;
        this.sources = new ArrayList<>();
        this.series = new ArrayList<>();
        this.movies = new Movies();
        LOGGER.traceExit();
    }

    public LogConfig getLogConfig() {
        return logConfig;
    }

    public Config setLogConfig(LogConfig logConfig) {
        this.logConfig = logConfig;
        return this;
    }

    public int getDelay() {
        return delay;
    }

    public Config setDelay(int delay) {
        this.delay = delay;
        return this;
    }

    public List<String> getSources() {
        LOGGER.traceEntry("getSources");
        return LOGGER.traceExit(this.sources);
    }

    public void setSources(List<String> sources) {
        LOGGER.traceEntry("setSources", sources);
        this.sources = sources;
        LOGGER.traceExit();
    }

    public Config addSource(String source) {
        LOGGER.traceEntry("addSource", source);
        this.sources.add(source);
        return LOGGER.traceExit(this);
    }

    public Optional<Series> getSeriesByName(String name) {
        return this.series.stream().filter(t -> t.getName().equals(name)).findFirst();
    }

    public List<Series> getSeries() {
        return series;
    }

    public void setSeries(List<Series> series) {
        this.series = series;
    }

    public Config addSeries(Series series) {
        this.series.add(series);
        return this;
    }

    public boolean validate() {
        boolean result = !this.sources.isEmpty();
        result = result && !this.series.isEmpty();
        return result;
    }

    public Optional<Series> getMatchingSerie(FileHolder file) {
        Optional<Series> result = Optional.empty();
        for( Series series : this.series ) {
            if( series.match(file.getSource()) && !result.isPresent() ) {
                result = Optional.of(series);
            } else if ( series.match(file.getSource()) ) {
                LOGGER.error("Duplicate matching for series: " + result.get().getName() +
                        " and " + series.getName() + "!");
                return Optional.empty();
            }
        }
        return result;
    }

    public boolean isMatchingMovie(FileHolder file) {
        Double size;
        try {
            size = Double.valueOf(file.size().replace("GB", ""));
        } catch (NumberFormatException e) {
            LOGGER.error("Received NumberFormatException from size on file.", e);
            size = 0.0;
        }
        boolean result = 0 < this.movies.getSizeGB();
        result = result && (size > movies.getSizeGB());
        return result;
    }

    public Config setMovies(Movies movies) {
        this.movies = movies;
        return this;
    }

    public Movies getMovies() {
        return this.movies;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.delay, this.logConfig, this.sources, this.series);
    }

    @Override
    public boolean equals(Object other) {
        if( other == null ) return false;
        if( this.getClass() != other.getClass() ) return false;
        final Config that = (Config)other;
        return Objects.equals(this.delay, that.delay) &&
                Objects.equals(this.logConfig, that.logConfig) &&
                Objects.equals(this.sources, that.sources) &&
                Objects.equals(this.series, that.series);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(Config.class)
                .add("delay", this.delay)
                .add("sources", this.sources)
                .add("series", this.series)
                .toString();
    }

}
