package io.scrier.json;

import com.google.common.base.MoreObjects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by scrier on 25/09/16.
 */
public class Series {

    private String name;
    private String matcher;
    private List<String> ignore;
    private String target;

    public Series() {
        this.name = "";
        this.matcher = "";
        this.ignore = new ArrayList<>();
        this.target = "";
    }

    public Series init() {
        this.matcher = regexify(this.matcher);
        List<String> toIgnore = new ArrayList<>(this.ignore);
        this.ignore.clear();
        for( String item : toIgnore ) {
            this.ignore.add(regexify(item));
        }
        return this;
    }

    public boolean match(File file) {
        boolean result = matcher(matcher, file.getName());
        for( String item : this.ignore ) {
            result = result && !matcher(item, file.getName());
        }
        return result;
    }

    public static boolean matcher(String regex, CharSequence input) {
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public String regexify(String input) {
        input = input.startsWith(".*") ? input : ".*" + input;
        input = input.endsWith(".*") ? input : input + ".*";
        return input;
    }

    public String getName() {
        return this.name;
    }

    public Series setName(String name) {
        this.name = name;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public Series setTarget(String target) {
        this.target = target;
        return this;
    }

    public List<String> getIgnore() {
        return ignore;
    }

    public Series addIgnore(String ignore) {
        this.ignore.add(ignore);
        return this;
    }

    public Series setIgnore(List<String> ignore) {
        this.ignore = ignore;
        return this;
    }

    public String getMatcher() {
        return matcher;
    }

    public Series setMatcher(String matcher) {
        this.matcher = matcher;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.matcher, this.ignore, this.target);
    }

    @Override
    public boolean equals(Object other) {
        if( other == null ) return false;
        if( this.getClass() != other.getClass() ) return false;
        final Series that = (Series)other;
        return Objects.equals(this.name, that.name) &&
                Objects.equals(this.matcher, that.matcher) &&
                Objects.equals(this.ignore, that.ignore) &&
                Objects.equals(this.target, that.target);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(Series.class)
                .add("name", this.name)
                .add("matcher", this.matcher)
                .add("ignore", this.ignore)
                .add("target", this.target)
                .toString();
    }

}
