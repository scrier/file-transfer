package io.scrier.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Created by scrier on 2016-10-07.
 */
public class LogConfig {

    private static final Logger LOGGER = LogManager.getLogger(LogConfig.class);

    @JsonProperty("source-files")
    private boolean sourceFiles;
    @JsonProperty("missed-files")
    private boolean missedFiles;
    @JsonProperty("copied-files")
    private boolean copiedFiles;
    @JsonProperty("debug-file-operations")
    private boolean debugFileOperations;

    public LogConfig() {
        LOGGER.trace("LogConfig()");
        setSourceFiles(false);
        setMissedFiles(false);
        setCopiedFiles(false);
    }

    public boolean isSourceFiles() {
        return sourceFiles;
    }

    public LogConfig setSourceFiles(boolean sourceFiles) {
        this.sourceFiles = sourceFiles;
        return this;
    }

    public boolean isMissedFiles() {
        return missedFiles;
    }

    public LogConfig setMissedFiles(boolean missedFiles) {
        this.missedFiles = missedFiles;
        return this;
    }

    public boolean isCopiedFiles() {
        return copiedFiles;
    }

    public LogConfig setCopiedFiles(boolean copiedFiles) {
        this.copiedFiles = copiedFiles;
        return this;
    }

    public boolean isDebugFileOperations() {
        return debugFileOperations;
    }

    public LogConfig setDebugFileOperations(boolean debugFileOperations) {
        this.debugFileOperations = debugFileOperations;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.sourceFiles, this.missedFiles, this.copiedFiles, this.debugFileOperations);
    }

    @Override
    public boolean equals(Object other) {
        if( other == null ) return false;
        if( this.getClass() != other.getClass() ) return false;
        final LogConfig that = (LogConfig)other;
        return Objects.equals(this.sourceFiles, that.sourceFiles) &&
                Objects.equals(this.missedFiles, that.missedFiles) &&
                Objects.equals(this.copiedFiles, that.copiedFiles) &&
                Objects.equals(this.debugFileOperations, that.debugFileOperations);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(LogConfig.class)
                .add("sourceFiles", this.sourceFiles)
                .add("missedFiles", this.missedFiles)
                .add("copiedFiles", this.copiedFiles)
                .add("debugFileOperations", this.debugFileOperations)
                .toString();
    }

}
